# README #

This is a Magento 2 demo module for out tutorials
@ [gladd.gr](https://www.gladd.gr/blog)

- [Δημιουργία module για το Magento 2](https://www.gladd.gr/blog/create-magento2-module)

[Empty module files](https://github.com/gladdgr/demo/tree/Empty-Module)

- [Δημιουργία EAV Entity για το Magento 2](https://www.gladd.gr/blog/create-magento2-eav-entity)

[EAV Entity files](https://github.com/gladdgr/demo/tree/EAV-Model)
