<?php
/*
 * Copyright © 2017 gladd. All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Gladd_Demo',
    __DIR__
);
